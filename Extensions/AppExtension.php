<?php
namespace extensions;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Exception;

class AppExtension extends AbstractExtension
{
    private $API_KEY = "K5HBBzpSnwGe6p_Elgxi2Yn28qy5WPk6xvhvOjSbk7W2BaeP_dLzTdniB2OXKKMQN_GND3WLKwW71KFHlvCeNztPd-9S4bS3ZIbTSni08YBM8DxnKAk8Ttw0_IAHYXYx";
    
    private $API_HOST = "https://api.yelp.com";
    private $SEARCH_PATH = "/v3/businesses/search";

    private $DEFAULT_TERM = "restaurant";
    private $DEFAULT_LOCATION = "Melbourne CBD, VIC";
    private $SEARCH_LIMIT = 20;

    public function request($host, $path, $url_params = array()) {
        // Send Yelp API Call
        try {
            $curl = curl_init();
            if (FALSE === $curl)
                throw new Exception('Failed to initialize');
    
            $url = $host . $path . "?" . http_build_query($url_params);
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,  
                CURLOPT_ENCODING => "",  
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "authorization: Bearer " . $this->API_KEY,
                    "cache-control: no-cache",
                ),
            ));
    
            $response = curl_exec($curl);
    
            if (FALSE === $response)
                throw new Exception(curl_error($curl), curl_errno($curl));
            $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if (200 != $http_status)
                throw new Exception($response, $http_status);
    
            curl_close($curl);
        } catch(Exception $e) {
            trigger_error(sprintf(
                'Curl failed with error #%d: %s',
                $e->getCode(), $e->getMessage()),
                E_USER_ERROR);
        }
    
        return $response;
    }

    public function search($term, $location) {
        $url_params = array();
        
        $url_params['term'] = $term;
        $url_params['location'] = $location;
        $url_params['limit'] = $this->SEARCH_LIMIT;
        
        return $this->request($this->API_HOST, $this->SEARCH_PATH, $url_params);
    }
    

    public function query_api() {   
        $term = $this->DEFAULT_TERM;
        $location = $this->DEFAULT_LOCATION;

        $response = json_decode($this->search($term, $location));
        // var_dump($response);exit();

        // $response = [
        //     'test1',
        //     'test2',
        //     'test3',
        // ];
    
        return $response;
       
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('yelpapi', [$this, 'query_api'])
        ];
    }
}